// KinectViewer.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <OpenNI.h>
#include <iostream>
#include <cstring>

using namespace std;
using namespace openni;

void OpenNIVersion() {
    cout << "OpenNI version is " << OpenNI::getVersion().major << "." 
        << OpenNI::getVersion().minor << "."
        << OpenNI::getVersion().maintenance << "."
        << OpenNI::getVersion().build << endl;
}

void UserInputHandler() {
    cout << "Press any key to continue..." << endl;
    cin.get();
}

bool HandleStatus(Status& theStatus) {

    if (theStatus == STATUS_OK)
        return true;

    cout << "ERROR: #" << theStatus << OpenNI::getExtendedError() << endl;
    UserInputHandler();
    return false;
}

bool DevicesInfo() {
    Array<DeviceInfo> aListOfDevices;
    OpenNI::enumerateDevices(&aListOfDevices);
    int aNumOfDevices = aListOfDevices.getSize();
    if (aNumOfDevices > 0) {
        for (int i = 0; i < aNumOfDevices; ++i) {
            DeviceInfo aDevice = aListOfDevices[i];
            cout << aDevice.getVendor() << " " << aDevice.getName()
                << " VID: " << aDevice.getUsbVendorId()
                << " PID: " << aDevice.getUsbProductId()
                << " is connected at " << aDevice.getUri() << endl;
        }
    } else {
        cout << "No devices connected.";
    }
    return true;
}

string KinectUri() {
    Array<DeviceInfo> aListofDevices;
    OpenNI::enumerateDevices(&aListofDevices);
    int aDeviceNum = aListofDevices.getSize();
    string aKinectUri;
    if (aDeviceNum > 0) {
        for (int i = 0; i < aDeviceNum; ++i) {
            if (!strcmp(aListofDevices[i].getName(), "Kinect" )) {
                aKinectUri = string(aListofDevices[i].getUri());
                return aKinectUri;
            }
        }
    }
    return aKinectUri;
}

void DisplayVideoMode (const VideoMode &theVM) {
    cout << theVM.getResolutionX() << " x "
        << theVM.getResolutionY() << " at "
        << theVM.getFps() << " fps with "
        << theVM.getPixelFormat() << " pixel format.\n";
}



int main(int argc, char* argv[])
{
    OpenNIVersion();
    cout << "Scanning devices...\r\n";

    Status aStatus = STATUS_OK;
    aStatus = OpenNI::initialize();
    if (!HandleStatus (aStatus)) {
        return 1;
    }
    cout << "Completed." << endl;
    DevicesInfo();
    string aKinectUri = KinectUri();
    if (aKinectUri.empty()) {
        cout << "Error: Can't find a connected Kinect.\n";
        UserInputHandler();
        return 1;
    }
    Device aKinect;
    aStatus = aKinect.open (aKinectUri.c_str());
    if (!HandleStatus (aStatus)) {
        return 1;
    }
    cout << "Kinect successfuly connected.\n";

    cout << "Checking is Kinect has depth sensor...\n";
    if (!aKinect.hasSensor (SENSOR_DEPTH)) {
        cout << "Error: depth sensor is unavaliable.\n";
        UserInputHandler();
        return 1;
    }
    cout << "Trying to get videostream...\n";
    VideoStream aVS;
    aStatus = aVS.create (aKinect,SENSOR_DEPTH);
    if (!HandleStatus (aStatus)) {
        return 1;
    }
    const Array<VideoMode>& aSupportedVMs = aVS.getSensorInfo().getSupportedVideoModes();
    int aSVMASize = aSupportedVMs.getSize();
    if (aSVMASize < 1) {
        cout << "Error: no supported video modes.\n";
        UserInputHandler();
        return 1;
    }
    int aVMCounter = 1;
    for (int i = 0; i < aSVMASize; ++i) {
        cout << aVMCounter+i << ". ";
        DisplayVideoMode (aSupportedVMs[i]);
    }

    cout << "Please, select a video mode:";
    int aUserVM;
    cin >> aUserVM;
    while ( !(aUserVM > aVMCounter) && !(aUserVM < 0) ) {
        cout << "Error: not existed videomode. Please enter another one:";
        cin >> aUserVM;
    }
    VideoMode aSelectedVM = aSupportedVMs [aUserVM - 1];
    cout << "Loading selected video stream: ";
    DisplayVideoMode(aSelectedVM);

    aVS.setVideoMode(aSelectedVM);
    aStatus = aVS.start();
    if (!HandleStatus (aStatus)) {
        return 1;
    }
    cout << "Selected video stream successfuly started.\n";

    aVS.destroy();
    aKinect.close();
    OpenNI::shutdown();
    UserInputHandler();

    return 0;
}

